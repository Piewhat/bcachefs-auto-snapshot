## Install
```
# make install
# systemctl daemon-reload
# systemctl enable --now bcachefs-auto-snapshot.timer
```

snapshots are created in `subvolume/.snapshot`

### Requirements:
[nushell](https://www.nushell.sh/book/installation.html)

## Configure
```
# cp /etc/bcachefs-auto-snapshot.toml.example /etc/bcachefs-auto-snapshot.toml
# vi /etc/bcachefs-auto-snapshot.toml
```

```
["/mnt/bcachefs/subvolume"] # path to subvolume
  frequently = 4 # 15 minutes
  hourly = 5
  daily = 3
  monthly = 2
  yearly = 1
```

## Uninstall
```
# make uninstall
```
