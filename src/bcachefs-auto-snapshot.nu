#!/usr/bin/env nu

def main [config: path = "/etc/bcachefs-auto-snapshot.toml"] {
  (open $config | transpose path config) | par-each {|subvolume| 
    print ("taking snapshots: [" + $subvolume.path + "]")
    snapshot $subvolume.path $subvolume.config (date now)
    print ("pruning snapshots: [" + $subvolume.path + "]")
    prune $subvolume.path $subvolume.config
  } | null
}

def snapshot [subvolume: string, config: record date: datetime] {
  mkdir ($subvolume + "/.snapshot")
  let snapshots = (ls ($subvolume + "/.snapshot/"))
  let frequently = ($snapshots | filter {|s| $s.name | str ends-with "_frequently"} | last 1)
  let hourly = ($snapshots  | filter {|s| $s.name | str ends-with "_hourly"} | last 1)
  let daily = ($snapshots | filter {|s| $s.name | str ends-with "_daily"} | last 1)
  let monthly = ($snapshots | filter {|s| $s.name | str ends-with "_monthly"} | last 1)
  let yearly = ($snapshots | filter {|s| $s.name | str ends-with "_yearly"} | last 1)
  let date_rec = ($date | date to-record)
  let preferred_frequently = $date_rec.minute mod 15 == 0
  let preferred_hourly = $date_rec.minute == 0
  let preferred_daily = $date_rec.hour == 0 and $preferred_hourly
  let preferred_monthly = $date_rec.day == 1 and $preferred_daily
  let preferred_yearly = $date_rec.month == 1 and $preferred_monthly
  let take_frequently = $preferred_frequently or ($frequently | length) == 0 and $config.frequently > 0
  let take_hourly = $preferred_hourly or ($hourly | length) == 0 and $config.hourly > 0
  let take_daily = $preferred_daily or ($daily | length) == 0 and $config.daily > 0
  let take_monthly = $preferred_monthly or ($monthly | length) == 0 and $config.monthly > 0
  let take_yearly = $preferred_yearly or ($yearly | length) == 0 and $config.yearly > 0
  
  if $take_frequently {
    bcachefs subvolume snapshot -r $subvolume ($subvolume + "/.snapshot/" + "autosnap_" + ($date | format date "%Y-%m-%d_%H:%M:%S") + "_frequently")
  }
  if $take_hourly {
    bcachefs subvolume snapshot -r $subvolume ($subvolume + "/.snapshot/" + "autosnap_" + ($date | format date "%Y-%m-%d_%H:%M:%S") + "_hourly")
  }
  if $take_daily {
    bcachefs subvolume snapshot -r $subvolume ($subvolume + "/.snapshot/" + "autosnap_" + ($date | format date "%Y-%m-%d_%H:%M:%S") + "_daily")
  }
  if $take_monthly {
    bcachefs subvolume snapshot -r $subvolume ($subvolume + "/.snapshot/" + "autosnap_" + ($date | format date "%Y-%m-%d_%H:%M:%S") + "_monthly")
  }
  if $take_yearly {
    bcachefs subvolume snapshot -r $subvolume ($subvolume + "/.snapshot/" + "autosnap_" + ($date | format date "%Y-%m-%d_%H:%M:%S") + "_yearly")
  }
}

def prune [subvolume: string, config: record] {
  let snapshots = (ls ($subvolume + "/.snapshot/"))
  let frequently = ($snapshots | filter {|s| $s.name | str ends-with "_frequently"} | drop $config.frequently | filter {|s| is_snap_old $s (-15min * $config.frequently)})
  let hourly = ($snapshots  | filter {|s| $s.name | str ends-with "_hourly"} | drop $config.hourly | filter {|s| is_snap_old $s (-1hr * $config.hourly)})
  let daily = ($snapshots | filter {|s| $s.name | str ends-with "_daily"} | drop $config.daily | filter {|s| is_snap_old $s (-1day * $config.daily)})
  let monthly = ($snapshots | filter {|s| $s.name | str ends-with "_monthly"} | drop $config.monthly | filter {|s| is_snap_old $s (-30day * $config.monthly)})
  let yearly = ($snapshots | filter {|s| $s.name | str ends-with "_yearly"} | drop $config.yearly | filter {|s| is_snap_old $s (-365day * $config.yearly)})
  let old_snapshots = ($frequently | append $hourly | append $daily | append $yearly)
        
  for snapshot in $old_snapshots {
    bcachefs subvolume delete $snapshot.name
  }
}

def is_snap_old [snapshot: record, duration: duration] {
  let snapshot_date = ($snapshot.name | split row '_' | last 3 | first 2 | str join T | into datetime)
  $snapshot_date - (date now) < $duration - -1min
}
