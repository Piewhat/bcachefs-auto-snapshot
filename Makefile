PREFIX := /usr/local

all:

install:
	install -m 0644 etc/bcachefs-auto-snapshot.toml $(DESTDIR)/etc/bcachefs-auto-snapshot.toml.example
	install -m 0644 etc/bcachefs-auto-snapshot.service $(DESTDIR)/etc/systemd/system/bcachefs-auto-snapshot.service
	install -m 0644 etc/bcachefs-auto-snapshot.timer $(DESTDIR)/etc/systemd/system/bcachefs-auto-snapshot.timer
	install src/bcachefs-auto-snapshot.nu $(DESTDIR)$(PREFIX)/sbin/bcachefs-auto-snapshot

uninstall:
	rm $(DESTDIR)/etc/systemd/system/bcachefs-auto-snapshot.service
	rm $(DESTDIR)/etc/systemd/system/bcachefs-auto-snapshot.timer
	rm $(DESTDIR)$(PREFIX)/sbin/bcachefs-auto-snapshot